-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 10, 2015 at 04:20 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_anest`
--

-- --------------------------------------------------------

--
-- Table structure for table `db_address`
--

CREATE TABLE IF NOT EXISTS `db_address` (
  `address_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_status` int(11) DEFAULT NULL,
  `address_registed` datetime DEFAULT NULL,
  `address_date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`address_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `db_address`
--

INSERT INTO `db_address` (`address_id`, `address_name`, `address_status`, `address_registed`, `address_date_delete`) VALUES
(1, 'ไม่ทราบสถานที่ใช้งาน', 0, '2015-07-04 18:55:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `db_borroweback`
--

CREATE TABLE IF NOT EXISTS `db_borroweback` (
  `borroweback_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `borroweback_id_link_equip` bigint(20) DEFAULT NULL,
  `borroweback_nametechborrowe` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `borroweback_nameborrower` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `borroweback_address_use` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `borroweback_detail_etc` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `borroweback_date_borrowe` datetime DEFAULT NULL,
  `borroweback_time_borrowe` time DEFAULT NULL,
  `borroweback_id_link_techrecord` bigint(20) DEFAULT NULL,
  `borroweback_status` int(11) DEFAULT NULL,
  `borroweback_registed` datetime DEFAULT NULL,
  `borroweback_date_back` datetime DEFAULT NULL,
  `borroweback_time_back` time DEFAULT NULL,
  `borroweback_nameborrowerback` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `borroweback_nametechback` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `borroweback_borrowestatus` int(11) DEFAULT NULL,
  `borroweback_date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`borroweback_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `db_equipment_gen`
--

CREATE TABLE IF NOT EXISTS `db_equipment_gen` (
  `equipment_gen_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `equipment_gen_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipment_gen_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipment_gen_brand` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipment_gen_model` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipment_gen_budget` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipment_gen_date_begin` datetime DEFAULT NULL,
  `equipment_gen_date_end` datetime DEFAULT NULL,
  `equipment_gen_company` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipment_gen_company_tel` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipment_gen_company_fax` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipment_gen_company_sales` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipment_gen_company_sales_tel` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipment_gen_price` bigint(20) DEFAULT NULL,
  `equipment_id_link_group` bigint(20) DEFAULT NULL,
  `equipment_gen_registed` datetime DEFAULT NULL,
  `equipment_id_link_pic` bigint(20) DEFAULT NULL,
  `equipment_gen_status` int(11) DEFAULT NULL,
  `equipment_gen_date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`equipment_gen_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `db_equipment_gen`
--

INSERT INTO `db_equipment_gen` (`equipment_gen_id`, `equipment_gen_name`, `equipment_gen_code`, `equipment_gen_brand`, `equipment_gen_model`, `equipment_gen_budget`, `equipment_gen_date_begin`, `equipment_gen_date_end`, `equipment_gen_company`, `equipment_gen_company_tel`, `equipment_gen_company_fax`, `equipment_gen_company_sales`, `equipment_gen_company_sales_tel`, `equipment_gen_price`, `equipment_id_link_group`, `equipment_gen_registed`, `equipment_id_link_pic`, `equipment_gen_status`, `equipment_gen_date_delete`) VALUES
(1, 'เครื่องดมยาสลบ', '12150000-D-65150010001/2551', 'DATEX-OHMEDA', 'Aestiva 5/7900', 'เงินธงวันมหิดลปี 2550', '2008-05-21 00:00:00', '2011-05-20 00:00:00', 'บริษัท สุพรีม โพรดักส์ จำกัด', '024340040', '024333971', 'คุณโสภณ สมบัติ', '0', 1500000, 1, '2015-07-06 10:59:30', 1, 0, NULL),
(2, 'เครื่องอุ่นร่างกายด้วยลมร้อน', '12150000-D-51300100002/2554', 'NELLCOR', '5900', 'เงินธงวันมหิดลปี 2553', '2011-06-01 00:00:00', '2016-05-31 00:00:00', 'บริษัท ซิลลิค ฟาร์มา จำกัด', '026569800', '026569801', 'คุณกรพิณ แก้ววรรณศิริ', '0', 55000, 5, '2015-07-07 03:52:34', 2, 0, NULL),
(3, 'เครื่องวิเคราะห์ก๊าซ', '12150000-D-65150170002/2551', 'DRAGER', 'VAMOS', 'เงินธงวันมหิดล ปี 2550', '2008-06-23 00:00:00', '2010-06-22 00:00:00', 'บริษัท เมดิทอป จำกัด', '029331133', '029330811', 'คุณเอกชัย อาชววาณิชกุล', '0', 100000, 7, '2015-07-07 04:02:15', 3, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `db_equipment_list`
--

CREATE TABLE IF NOT EXISTS `db_equipment_list` (
  `equipment_list_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `equipment_list_sap` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipment_list_sn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipment_list_tel` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipment_list_address_link` bigint(20) DEFAULT NULL,
  `equipment_list_status_link` bigint(20) DEFAULT NULL,
  `equipment_list_status` int(11) DEFAULT NULL,
  `equipment_list_gen_link` bigint(20) DEFAULT NULL,
  `equipment_list_date_delete` datetime DEFAULT NULL,
  `equipment_list_registed` datetime DEFAULT NULL,
  PRIMARY KEY (`equipment_list_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

--
-- Dumping data for table `db_equipment_list`
--

INSERT INTO `db_equipment_list` (`equipment_list_id`, `equipment_list_sap`, `equipment_list_sn`, `equipment_list_tel`, `equipment_list_address_link`, `equipment_list_status_link`, `equipment_list_status`, `equipment_list_gen_link`, `equipment_list_date_delete`, `equipment_list_registed`) VALUES
(1, '131000020408', 'AMRM00463', '0', 1, 1, 0, 1, NULL, '2015-07-06 10:59:30'),
(2, '131000020409', 'AMRM00464', '0', 1, 1, 0, 1, NULL, '2015-07-06 10:59:30'),
(3, '131000020410', 'AMRM00465', '0', 1, 1, 0, 1, NULL, '2015-07-06 10:59:31'),
(4, '131000020411', 'AMRM00466', '0', 1, 1, 0, 1, NULL, '2015-07-06 10:59:31'),
(5, '131000027279', 'CI0610J295', '0', 1, 1, 0, 2, NULL, '2015-07-07 03:52:34'),
(6, '131000027280', 'CI0610J296', '0', 1, 1, 0, 2, NULL, '2015-07-07 03:52:34'),
(7, '131000027281', 'CI0610J297', '0', 1, 1, 0, 2, NULL, '2015-07-07 03:52:34'),
(8, '131000027282', 'CI0610J298', '0', 1, 1, 0, 2, NULL, '2015-07-07 03:52:34'),
(9, '131000027283', 'CI0610J299', '0', 1, 1, 0, 2, NULL, '2015-07-07 03:52:34'),
(10, '131000027284', 'CI0610J300', '0', 1, 1, 0, 2, NULL, '2015-07-07 03:52:34'),
(11, '183000009867', 'ARYM-0071', '0', 1, 1, 0, 3, NULL, '2015-07-07 04:02:15'),
(12, '183000009868', 'ARYM-0073', '0', 1, 1, 0, 3, NULL, '2015-07-07 04:02:15'),
(13, '183000009869', 'ARYM-0074', '0', 1, 1, 0, 3, NULL, '2015-07-07 04:02:15'),
(14, '183000009870', 'ARYM-0080', '0', 1, 1, 0, 3, NULL, '2015-07-07 04:02:15'),
(15, '183000009871', 'ARYM-0081', '0', 1, 1, 0, 3, NULL, '2015-07-07 04:02:15'),
(16, '183000009872', 'ARYM-0086', '0', 1, 1, 0, 3, NULL, '2015-07-07 04:02:15'),
(17, '183000009873', 'ARYM-0098', '0', 1, 1, 0, 3, NULL, '2015-07-07 04:02:16'),
(18, '183000009874', 'ARYM-0099', '0', 1, 1, 0, 3, NULL, '2015-07-07 04:02:16'),
(19, '183000009875', 'ARZF-0002', '0', 1, 1, 0, 3, NULL, '2015-07-07 04:02:16'),
(20, '183000009876', 'ARZF-0003', '0', 1, 1, 0, 3, NULL, '2015-07-07 04:02:16'),
(21, '183000009877', 'ARZF-0004', '0', 1, 1, 0, 3, NULL, '2015-07-07 04:02:16'),
(22, '183000009878', 'ARZF-0005', '0', 1, 1, 0, 3, NULL, '2015-07-07 04:02:16');

-- --------------------------------------------------------

--
-- Table structure for table `db_equipment_pic`
--

CREATE TABLE IF NOT EXISTS `db_equipment_pic` (
  `equipment_pic_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `equipment_pic_name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipment_pic_tmp` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipment_pic_type` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipment_pic_size` bigint(20) DEFAULT NULL,
  `equipment_pic_error` int(11) DEFAULT NULL,
  `equipment_pic_status` int(11) DEFAULT NULL,
  `equipment_pic_registed` datetime DEFAULT NULL,
  PRIMARY KEY (`equipment_pic_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `db_equipment_pic`
--

INSERT INTO `db_equipment_pic` (`equipment_pic_id`, `equipment_pic_name`, `equipment_pic_tmp`, `equipment_pic_type`, `equipment_pic_size`, `equipment_pic_error`, `equipment_pic_status`, `equipment_pic_registed`) VALUES
(1, '7900.jpg', 'D:\\Install\\Xampp\\tmp\\phpDAF9.tmp', 'image/jpeg', 136489, 0, 0, '2015-07-06 10:59:29'),
(2, '5900.jpg', 'D:\\Install\\Xampp\\tmp\\phpE6C5.tmp', 'image/jpeg', 5506, 0, 0, '2015-07-07 03:52:34'),
(3, 'vamos.jpg', 'D:\\Install\\Xampp\\tmp\\phpC4E5.tmp', 'image/jpeg', 7425, 0, 0, '2015-07-07 04:02:15');

-- --------------------------------------------------------

--
-- Table structure for table `db_group`
--

CREATE TABLE IF NOT EXISTS `db_group` (
  `group_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_maintanance` int(11) DEFAULT NULL,
  `group_status` int(11) DEFAULT NULL,
  `group_registed` datetime DEFAULT NULL,
  `group_date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `db_group`
--

INSERT INTO `db_group` (`group_id`, `group_name`, `group_maintanance`, `group_status`, `group_registed`, `group_date_delete`) VALUES
(1, 'Machine', 6, 0, '2015-07-02 17:24:29', NULL),
(2, 'Monitor', 6, 0, '2015-07-02 17:24:41', NULL),
(3, 'Syringe pump', 6, 0, '2015-07-02 17:33:58', NULL),
(4, 'Infusion pump', 6, 0, '2015-07-02 17:34:22', NULL),
(5, 'Air warmer', 4, 0, '2015-07-02 17:40:36', NULL),
(6, 'Fluid warmer', 2, 0, '2015-07-02 17:40:48', NULL),
(7, 'Agent monitor', 4, 0, '2015-07-07 03:55:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `db_inventory`
--

CREATE TABLE IF NOT EXISTS `db_inventory` (
  `inventory_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `inventory_link_equip` bigint(20) DEFAULT NULL,
  `inventory_note` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inventory_date` datetime DEFAULT NULL,
  `inventory_about` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inventory_address_store` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inventory_link_tech` bigint(20) DEFAULT NULL,
  `inventory_status` int(11) DEFAULT NULL,
  `inventory_registed` datetime DEFAULT NULL,
  `inventory_date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`inventory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `db_position`
--

CREATE TABLE IF NOT EXISTS `db_position` (
  `position_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `position_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position_status` int(11) DEFAULT NULL,
  `position_registed` datetime DEFAULT NULL,
  `position_date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `db_position`
--

INSERT INTO `db_position` (`position_id`, `position_name`, `position_status`, `position_registed`, `position_date_delete`) VALUES
(1, 'อาจารย์แพทย์', 0, '2015-07-02 17:44:11', NULL),
(2, 'แพทย์ประจำบ้าน', 0, '2015-07-02 17:44:22', NULL),
(3, 'วิสัญญีพยาบาล', 0, '2015-07-02 17:44:38', NULL),
(4, 'นักเรียนวิสัญญีพยาบาล', 0, '2015-07-02 17:45:02', NULL),
(5, 'นักวิทยาศาสตร์', 0, '2015-07-02 17:45:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `db_repair`
--

CREATE TABLE IF NOT EXISTS `db_repair` (
  `repair_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `repair_link_pic` bigint(20) DEFAULT NULL,
  `repair_link_list` bigint(20) DEFAULT NULL,
  `repair_date` datetime DEFAULT NULL,
  `repair_date_send` datetime DEFAULT NULL,
  `repair_social` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repair_about_send` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repair_registed` datetime DEFAULT NULL,
  `repair_tech_send` bigint(20) DEFAULT NULL,
  `repair_status` int(11) DEFAULT NULL,
  `repair_check` int(11) DEFAULT NULL,
  `repair_date_back` datetime DEFAULT NULL,
  `repair_social_tech` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repair_about_back` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repair_tech_back` bigint(20) DEFAULT NULL,
  `repair_cost` bigint(20) DEFAULT NULL,
  `repair_date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`repair_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `db_repair_pic`
--

CREATE TABLE IF NOT EXISTS `db_repair_pic` (
  `repair_pic_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `repair_pic_name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repair_pic_tmp` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repair_pic_type` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repair_pic_size` bigint(20) DEFAULT NULL,
  `repair_pic_error` int(11) DEFAULT NULL,
  `repair_pic_status` int(11) DEFAULT NULL,
  `repair_pic_registed` datetime DEFAULT NULL,
  PRIMARY KEY (`repair_pic_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `db_status`
--

CREATE TABLE IF NOT EXISTS `db_status` (
  `status_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_status` int(11) DEFAULT NULL,
  `status_registed` datetime DEFAULT NULL,
  `status_date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `db_status`
--

INSERT INTO `db_status` (`status_id`, `status_name`, `status_status`, `status_registed`, `status_date_delete`) VALUES
(1, 'พร้อมใช้งาน', 0, '2015-07-02 17:47:02', NULL),
(2, 'อยู่ระหว่างการส่งซ่อม', 0, '2015-07-02 17:47:18', NULL),
(3, 'รอการบำรุงรักษา', 0, '2015-07-06 10:53:10', NULL),
(4, 'รอการส่งคืนพัสดุ', 0, '2015-07-06 10:53:25', NULL),
(5, 'ส่งคืนพัสดุแล้ว', 0, '2015-07-07 04:38:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `db_switch`
--

CREATE TABLE IF NOT EXISTS `db_switch` (
  `switch_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `switch_link_equip` bigint(20) DEFAULT NULL,
  `switch_link_old_address` bigint(20) DEFAULT NULL,
  `switch_link_new_address` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `switch_address` int(11) DEFAULT NULL,
  `switch_note` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `switch_date` datetime DEFAULT NULL,
  `switch_link_id_tech` bigint(20) DEFAULT NULL,
  `switch_status` int(11) DEFAULT NULL,
  `switch_registed` datetime DEFAULT NULL,
  `switch_date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`switch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `db_user`
--

CREATE TABLE IF NOT EXISTS `db_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_lastname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_login` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_pass` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_tel` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_position` bigint(20) DEFAULT NULL,
  `user_gender` int(11) DEFAULT NULL,
  `user_account` int(11) DEFAULT NULL,
  `user_status` int(11) DEFAULT NULL,
  `user_registed` datetime DEFAULT NULL,
  `user_id_link_pic` bigint(20) DEFAULT NULL,
  `user_date_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `db_user`
--

INSERT INTO `db_user` (`user_id`, `user_name`, `user_lastname`, `user_login`, `user_pass`, `user_email`, `user_tel`, `user_position`, `user_gender`, `user_account`, `user_status`, `user_registed`, `user_id_link_pic`, `user_date_delete`) VALUES
(1, 'pinyoo', 'thotaboot', 'pinyoo_too', 'bc40c324b500a4dcd6849017b0efb1a1', 'pinyoo_too@hotmail.com', '0881829912', 3, 0, 2, 0, '2015-07-07 15:30:13', 18, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `db_user_pic`
--

CREATE TABLE IF NOT EXISTS `db_user_pic` (
  `user_pic_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_pic_name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_pic_tmp` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_pic_type` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_pic_size` bigint(20) DEFAULT NULL,
  `user_pic_error` int(11) DEFAULT NULL,
  `user_pic_status` int(11) DEFAULT NULL,
  `user_pic_registed` datetime DEFAULT NULL,
  PRIMARY KEY (`user_pic_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
